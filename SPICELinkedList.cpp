/*
 * SPICELinkedList.cpp
 *
 *  Created on: May 1, 2014
 *      Author: eprasetio
 */

#include "SPICELinkedList.h"

//constructor - Initialize first node
SPICELinkedList::SPICELinkedList() {
	size = 0; //init list's size
	head = 0;
	current = 0;
	maxNode1 = 0;
	maxNode2 = 0;
}

//desconstructor
SPICELinkedList::~SPICELinkedList() {

}

node* SPICELinkedList::createList(component* new_data) {
	node* ptr = (node*) malloc(sizeof(node));

	if (0 == ptr) {
		printf("\n Node creation failed\n");
		return NULL;
	}

	ptr->data = new_data;
	ptr->next = NULL;

	head = ptr;
	current = ptr;

	size++; //increment size by 1

	//initialize max node
	maxNode1 = new_data->node1;
	maxNode2 = new_data->node2;

	return ptr;
}

node* SPICELinkedList::addToList(component* new_data, int addToEnd) {
	if (0 == head) {
		return (createList(new_data));
	}

	node* ptr = (node*) malloc(sizeof(node));

	ptr->data = new_data;
	ptr->next = NULL;

	if (addToEnd) {
		current->next = ptr;
		current = ptr;
	} else {
		ptr->next = head;
		head = ptr;
	}

	size++; //increment size by 1

	//find max node number
	if (maxNode1 < new_data->node1) {
		maxNode1 = new_data->node1;
	}

	if (maxNode2 < new_data->node2) {
		maxNode2 = new_data->node2;
	}

	return ptr;
}

component* SPICELinkedList::getDataByPos(int pos) {
	int index = 0;
	node* searchCursor = head;

	while (index != pos) {
		if (searchCursor->next == NULL) {
			printf("Your index exceeds the list's size");
			break;
		} else {
			searchCursor = searchCursor->next;
		}
		index++;
	}

	return searchCursor->data;
}

node* SPICELinkedList::searchInList(char* target_name, node**prev) {
	node* searchCursor = head;
	node* tmp = NULL;

	while (searchCursor->next != NULL) {
		if (!strncmp(searchCursor->data->name, target_name,
				sizeof(target_name) - 1)) {
			if (prev)
				*prev = tmp;
			printf("Component is found at this position");
			break;
		} else {
			tmp = searchCursor;
			searchCursor = searchCursor->next;
		}
	}

	return searchCursor;
}

void SPICELinkedList::deleteFromList(char* target_name) {
	node* prevPtr;
	node* deletePtr = searchInList(target_name, &prevPtr);

	if (deletePtr == NULL) {
		puts("Node doesn't exist");
	} else {
		if (prevPtr != NULL) {
			prevPtr->next = deletePtr->next;
		} else if (deletePtr == current) {
			prevPtr->next = NULL;
		} else {
			head = deletePtr->next;
		}
	}

	free(deletePtr->data->name); //clean allocated memory for ptr to element's name
	deletePtr->data->name = NULL;
	free(deletePtr->data); //clean allocated memory for ptr to component's struct
	deletePtr->data = NULL;
	free(deletePtr); //clean allocated memory for ptr to node
	deletePtr = NULL;

	size--; //decrement size by 1

}

int SPICELinkedList::getListSize() {
	return size;
}

void SPICELinkedList::printList() {
	node* printCursor = head;
	int index = 0;

	printf("\n");
	do {
		printf("Data #%d:\n", index);
		printf("\tType: %d\n", printCursor->data->type);
		printf("\tName: %s\n", printCursor->data->name);
		printf("\tValue: %f\n", printCursor->data->value);
		printf("\tNode1: %d\n", printCursor->data->node1);
		printf("\tNode2: %d\n", printCursor->data->node2);
		printf("\tMode: %d\n", printCursor->data->mode);
		index++;
		printCursor = printCursor->next;
	} while (printCursor != NULL);
	printf("Out the loop\n");
}

int SPICELinkedList::getMaxNodeNumb() {
	int maxNode = maxNode1;

	if (maxNode1 < maxNode2) {
		maxNode = maxNode2;
	}

	return maxNode;
}
