/*
 * MatrixStampler.h
 *
 *  Created on: May 6, 2014
 *      Author: eprasetio
 */

#ifndef MATRIXSTAMPLER_H_
#define MATRIXSTAMPLER_H_

//#ifdef __cplusplus
//extern "C"
//#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "elements.h"
#include "SPICELinkedList.h"

# include "kernel.h"

class MatrixStampler {
public:
	SPICELinkedList dataList;
	double* componentMtrx;
	double* componentMtrxWoGnd;
	double* currentMtrx;
	double* currentMtrxWoGnd;
	double* voltageMtrx;
	double* voltageMtrxWoGnd;

	int row, column;

	MatrixStampler(SPICELinkedList, int, int);
	virtual ~MatrixStampler();

	void printComponentMtrx();
	void printComponentMtrxWoGnd();
	void printCurrentMtrx();
	void printCurrentMtrxWoGnd();
	void printVoltageMtrx();
	void printVoltageMtrxWoGnd();

private:
	void constructComponentMatrix();
	void constructCurrentMatrix();
	void constructVoltageMatrix();

	void zeroedVoltageMtrx(int);
	void zeroedVoltageMtrxWoGnd(int);
	void zeroedCurrentMtrx(int);
	void zeroedCurrentMtrxWoGnd(int);
	void zeroedComponentMtrx(int);
	void zeroedComponentMtrxWoGnd(int);

	void stampComponent();
	void solveForVoltageMatrix();
	void truncateGndRowColumn();


};

#endif /* MATRIXSTAMPLER_H_ */
