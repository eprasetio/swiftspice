#include "NetlistParser.h"

//NetlistParser Constructor
NetlistParser::NetlistParser(char* fileType, char* filePath) {
	index = 0; //initialize line index back to the first file

	//open the file
	FILE *targetFile = fopen(filePath, fileType); // use "rt" for reading *.txt/*.dat files

	//Error handling for null file
	if (targetFile == NULL) {
		perror("File does not exist!");
	}

	//start scanning the file
	for (int i = 0; i < BUFFER_SIZE; i++) {
		fscanf(targetFile, "%c", &netlistData[i]);
	}
	//close the file
	fclose(targetFile);

}

//NetlistParser De-constructor
NetlistParser::~NetlistParser() {

}

//method to print netlist data
void NetlistParser::printNetlistData() {

	printf("%s", netlistData);
	printf("\n\n");
}

//method for reading one line
void NetlistParser::readNextLine(char* strng) {

	//read one line
	while (netlistData[index] != '\n' && netlistData[index] != '\0') {
		*strng++ = netlistData[index++];
	}
	*strng = '\0'; //add termination char at the end

	//if reaching the end of the file, move index back to the first line
	if (netlistData[index] == '\0') {
		index = 0;
	} else {
		index++; //move to the next line
	}

}

//method for reading one char
void NetlistParser::readNextChar(char* chr) {
	strcpy(chr, &netlistData[index]);
	*(++chr) = '\0'; //add termination char at the end

	//if reaching the end of the file, move index back to the first line
	if (netlistData[index] == '\0') {
		index = 0;
	} else {
		index++; //move to the next char
	}
}

//method to check if next line is available
bool NetlistParser::hasNextLine() {
	if (netlistData[index] != '\0') {
		return true;
	} else {
		return false;
	}
}

//method to check if next char is available
bool NetlistParser::hasNextChar() {
	if (netlistData[index] != '\0') {
		return true;
	} else {
		return false;
	}
}

//method to parse netlist data
SPICELinkedList NetlistParser::parseNetlist() {
	char comment[SMALL_BUFFER_SIZE];
	char line[SMALL_BUFFER_SIZE];
	char element[ELEMENT_BUFFER_SIZE];
	component* myComponentStruct;

	readNextLine(comment); //read the first line in netlist = comment
	//printf("Comments: %s\n", comment);

	//if there's next line, keep reading the netlist
	bool hasNextLine = true;
	int line_index = 0;
	do {
		readNextLine(line); //read the second line in netlist
		//printf("Line #%d: %s\n", ++line_index, line);

		//if there's still elements available, keep reading elements
		int element_index = 0;
		bool hasNextElement = true;

		myComponentStruct = (component*) malloc(sizeof(component)) ; //initialize struct to store components
		do {
			cleanCharArray(element, sizeof(element));
			getElement(element, line, ++element_index);

			if (!strncmp(element, ".end", 4)) { //if reaching the end of the file, stop reading and parsing netlist
				//printf("\t\tReached the end of the file\n");
				hasNextElement = false;
				hasNextLine = false;
			} else if (*element == '\0') { //if reach the end of the line, end the loop
				//printf("\t\tReached end of the line\n");
				hasNextElement = false;
			} else {
				//detect element type from first element
				if (element_index == 1) {
					char* name_ptr = (char*) malloc(sizeof(element)); //allocate memory for element's name
					strcpy(name_ptr, element);

					//printf("\tElement #%d - Type: %d\n", element_index,detectElement(element));
					//printf("\tElement #%d - Name: %s\n", element_index,element);
					myComponentStruct->type = detectElement(element); //store element's type
					myComponentStruct->name = name_ptr; //store element's name
				} else if (element_index == 2) {
					//printf("\tElement #%d - node1: %s\n", element_index,element);
					myComponentStruct->node1 = atoi(element); //store element's node1
				} else if (element_index == 3) {
					//printf("\tElement #%d - node2: %s\n", element_index,element);
					myComponentStruct->node2 = atoi(element); //store element's node2
				} else if (element_index == 4) {
					//printf("\tElement #%d - value: %s\n", element_index,element);
					myComponentStruct->value = atof(element); //store element's value
				} else {
					//printf("\tElement #%d - mode: %d\n", element_index, 0);
					myComponentStruct->mode = NA; //store element's value
				}
			}

		} while (hasNextElement); //while element is not a white space


		//store the element in data structure - SPICELinkedList
		if (myComponentStruct->name != NULL && hasNextLine) { //check if element's name is not null
			if (line_index == 1) { //if list hasn't been created, create new linked list
				//printf("\t\tCreate List\n");
				myLinkedList.createList(myComponentStruct);
			} else {
				myLinkedList.addToList(myComponentStruct, 1); //if list has been created, add new element to the end of the list
				//printf("\t\tAdd to list\n");
			}
		}

	} while (hasNextLine); //while next line is available, keep reading it

	return myLinkedList;

}

el_type NetlistParser::detectElement(char* element) {
	el_type elementType = UNDEF;
	char elementSymb;

	strncpy(&elementSymb, element, 1); //get the first character to get element symbol

	//determine the element type
	switch (elementSymb) {
	case 'r':
		elementType = RESISTOR;
		break;
	case 'R':
		elementType = RESISTOR;
		break;
	case 'c':
		elementType = CAPACITOR;
		break;
	case 'C':
		elementType = CAPACITOR;
		break;
	case 'v':
		elementType = VS;
		break;
	case 'V':
		elementType = VS;
		break;
	case 'i':
		elementType = CS;
		break;
	case 'I':
		elementType = CS;
		break;
	default:
		elementType = UNDEF;
	}

	return elementType;
}

void NetlistParser::getElement(char* output, char* input, int position) {
	//get element at the position
	for (int i = 0; i < position; i++) {
		//read one line and get the element
		while (!isspace(*input) && *input != '\0') {
			if (i == (position - 1)) {
				*output++ = *input;
			}
			input++; //move cursor to next char
		}
		input++; //move cursor from whitespace char
	}
}

//helper function to clean char buffer
void NetlistParser::cleanCharArray(char* array, int size) {
	for (int i = 0; i < size; i++) {
		*array++ = 0;
	}
}

