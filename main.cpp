/*
 * main.c
 *
 *  Created on: Apr 28, 2014
 *      Author: eprasetio
 */
#ifdef __cplusplus

extern "C"

#endif

#include<stdio.h>
#include<string.h>
#include "NetlistParser.h"
#include "elements.h"
#include "MatrixStampler.h"

void cleanCharArray(char* array, int size) {
	for (int i = 0; i < size; i++) {
		*array++ = 0;
	}
}

int main() {
	char* netlistData;
	char file_path[] = {
			"/Users/eprasetio/cuda-workspace/SwiftSPICE/src/netlist.cir" };
	char file_type[] = { "rt" };
	char buffStr[1024];
	char element[1024];
	char buff;

	printf("========Begin===========\n\n");

	NetlistParser myNetlistParser(file_type, file_path);
	SPICELinkedList data = myNetlistParser.parseNetlist();
	//data.printList();
	//printf("List size: %d\n", data.getListSize());

	MatrixStampler myMtrx(data, 5, 5);
	//myMtrx.printComponentMtrx();
	//myMtrx.printCurrentMtrx();


	printf("\n\n===============================\n");

}

