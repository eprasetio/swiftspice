/*
 * kernel.h
 *
 *  Created on: May 8, 2014
 *      Author: eprasetio
 */

#ifndef KERNEL_H_
#define KERNEL_H_

#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime.h>
#include "cusparse.h"
#include "device_launch_parameters.h"

#define CLEANUP(s) \
do {\
	printf ("%s\n", s); \
	if (yHostPtr) free(yHostPtr); \
	if (xIndHostPtr) free(xIndHostPtr); \
	if (xValHostPtr) free(xValHostPtr); \
	if (cooRowIndexHostPtr) free(cooRowIndexHostPtr);\
	if (cooColIndexHostPtr) free(cooColIndexHostPtr);\
	if (cooValHostPtr) free(cooValHostPtr); \
	if (y) cudaFree(y); \
	if (xInd) cudaFree(xInd); \
	if (xVal) cudaFree(xVal); \
	if (csrRowPtr) cudaFree(csrRowPtr); \
	if (cooRowIndex) cudaFree(cooRowIndex); \
	if (cooColIndex) cudaFree(cooColIndex); \
	if (cooVal) cudaFree(cooVal); \
	if (descr) cusparseDestroyMatDescr(descr);\
	if (descrL) cusparseDestroyMatDescr(descrL);\
	if (descrU) cusparseDestroyMatDescr(descrU);\
	if (info) cudaFree(info); \
	if (inforL) cudaFree(inforL); \
	if (inforU) cudaFree(inforU); \
	if (handle) cusparseDestroy(handle); \
	cudaDeviceReset(); \
	fflush (stdout); \
}while (0)

#define CLEANUP2(s) \
do {\
	printf ("%s\n", s); \
	if (aDense) cudaFree(aDense); \
	if (nnzPerRow) cudaFree(aDense); \
	if (nnzTotalDevPtr) cudaFree(aDense); \
	if (handle) cusparseDestroy(handle); \
	if (descr) cusparseDestroyMatDescr(descr);\
	if (temp1) cudaFree(temp1); \
	if (temp2) cudaFree(temp2); \
	cudaDeviceReset(); \
	fflush (stdout); \
}while (0)

#define CLEANUP3(s) \
do {\
	printf ("%s\n", s); \
	if (cooRowIndexHostPtr) free(cooRowIndexHostPtr); \
	if (cooColIndexHostPtr) free(cooColIndexHostPtr); \
	if (cooValHostPtr) free(cooValHostPtr); \
	cudaDeviceReset(); \
	fflush (stdout); \
}while (0)

int AxEqBSolver(double *, double *, int, double *);
int getNonZero(double *, int, int[], int *);
int convertDenseToCooFormat(double *, int, int *, double*, int*, int*);
int linearSysSolverWithLUDec(int *, int *, double *, double *, int, int, double *);

#endif /* KERNEL_H_ */
