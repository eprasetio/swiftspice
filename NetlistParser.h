/*
 * NetlistParser.h
 *
 *  Created on: Apr 28, 2014
 *      Author: eprasetio
 */

#ifndef NETLISTPARSER_H_
#define NETLISTPARSER_H_

//#ifdef __cplusplus
//extern "C"
//#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "elements.h"
#include "SPICELinkedList.h"

#define BUFFER_SIZE 1024
#define SMALL_BUFFER_SIZE 80
#define ELEMENT_BUFFER_SIZE 20

class NetlistParser {
public:
	SPICELinkedList myLinkedList;

	///Instantiate the NetlistParser
	NetlistParser(char*, char*);
	~NetlistParser();

	SPICELinkedList parseNetlist();

private:
	int index;
	char netlistData[BUFFER_SIZE];

	void readNextLine(char*);
	void readNextChar(char*);
	bool hasNextLine();
	bool hasNextChar();
	void printNetlistData();
	void getElement(char*, char*, int);
	void cleanCharArray(char*, int);
	el_type detectElement(char*);

};

#endif /* NETLISTPARSER_H_ */
