/*
 * Elements.h
 *
 *  Created on: May 1, 2014
 *      Author: eprasetio
 */

#ifndef ELEMENTS_H_
#define ELEMENTS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef enum el_type_t {
	UNDEF, RESISTOR, DIODE, CAPACITOR, VS, CS
} el_type;

typedef enum el_mode_t {
	NA, DC, AC
} el_mode;

//struct for storing elements' parameters
struct component {
	el_type type;
	char* name;
	double value;
	int node1;
	int node2;
	el_mode mode;
};

struct node {
	component* data;
	node* next;
};


#endif /* ELEMENTS_H_ */




