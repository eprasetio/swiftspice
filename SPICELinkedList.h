/*
 * SPICELinkedList.h
 *
 *  Created on: May 1, 2014
 *      Author: eprasetio
 */

#ifndef SPICELINKEDLIST_H_
#define SPICELINKEDLIST_H_

//#ifdef __cplusplus
//extern "C"
//#endif

#include "Elements.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

class SPICELinkedList {

public:
	struct node* head;
	struct node* current;
	int size;
	int maxNode1;
	int maxNode2;

	SPICELinkedList();
	~SPICELinkedList();

	node* createList(component*);
	node* addToList(component*, int);
	component* getDataByPos(int);
	node* searchInList(char*, node**);
	void deleteFromList(char*);
	int getListSize();
	void printList();
	int getMaxNodeNumb();

private:

};

#endif /* SPICELINKEDLIST_H_ */
