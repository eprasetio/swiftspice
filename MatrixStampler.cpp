/*
 * MatrixStampler.cpp
 *
 *  Created on: May 6, 2014
 *      Author: eprasetio
 */

#include "MatrixStampler.h"

MatrixStampler::MatrixStampler(SPICELinkedList param_dataList, int param_row,
		int param_column) {
	componentMtrx = NULL;
	currentMtrx = NULL;
	voltageMtrx = NULL;

	dataList = param_dataList;

	row = dataList.getMaxNodeNumb() + 1;
	column = row;

	constructComponentMatrix();
	constructCurrentMatrix();
	constructVoltageMatrix();

	stampComponent();

	printComponentMtrx();
	printComponentMtrxWoGnd();
	printCurrentMtrx();
	printCurrentMtrxWoGnd();

	solveForVoltageMatrix();
	printVoltageMtrxWoGnd();

}

MatrixStampler::~MatrixStampler() {
	if (currentMtrx)
		free(currentMtrx);
	if (currentMtrxWoGnd)
		free(currentMtrxWoGnd);
	if (componentMtrx)
		free(componentMtrx);
	if (componentMtrxWoGnd)
		free(componentMtrxWoGnd);
	if (voltageMtrx)
		free(voltageMtrx);
	if (voltageMtrxWoGnd)
		free(voltageMtrxWoGnd);
}

void MatrixStampler::stampComponent() {
	component* cmpnt;
	for (int i = 0; i < dataList.getListSize(); i++) {
		cmpnt = dataList.getDataByPos(i);
		if (cmpnt->type == RESISTOR) { //if component is resistor, start stamping at component matrix
			componentMtrx[(cmpnt->node1 * column) + cmpnt->node1] += (1
					/ (cmpnt->value));
			componentMtrx[(cmpnt->node1 * column) + cmpnt->node2] += (-1
					/ (cmpnt->value));
			componentMtrx[(cmpnt->node2 * column) + cmpnt->node1] += (-1
					/ (cmpnt->value));
			componentMtrx[(cmpnt->node2 * column) + cmpnt->node2] += (1
					/ (cmpnt->value));
		} else if (cmpnt->type == CS) {
			currentMtrx[cmpnt->node1] += (-1 * cmpnt->value);
			currentMtrx[cmpnt->node2] += (cmpnt->value);
		}

	}

	truncateGndRowColumn();
}

// contruct component matrix in dense form
void MatrixStampler::constructComponentMatrix() {
	componentMtrx = (double*) malloc(row * column * sizeof(componentMtrx[0]));
	componentMtrxWoGnd = (double*) malloc(
			(row - 1) * (column - 1) * sizeof(componentMtrx[0]));
	if ((!componentMtrx) || (!componentMtrxWoGnd)) {
		printf("Comp mtrx malloc failed");
	} else {
		zeroedComponentMtrx(row * column);
		zeroedComponentMtrxWoGnd((row - 1) * (column - 1));
	}

}

void MatrixStampler::constructCurrentMatrix() {
	currentMtrx = (double*) malloc(row * sizeof(double));
	currentMtrxWoGnd = (double*) malloc((row - 1) * sizeof(double));

	if ((!currentMtrx) || (!currentMtrxWoGnd)) {
		printf("Current mtrx malloc failed");
	} else {
		zeroedCurrentMtrx(row);
		zeroedCurrentMtrxWoGnd(row - 1);
	}
}

void MatrixStampler::constructVoltageMatrix() {
	voltageMtrx = (double*) malloc(row * sizeof(double));
	voltageMtrxWoGnd = (double*) malloc((row - 1) * sizeof(double));

	if ((!voltageMtrx) || (!voltageMtrxWoGnd)) {
		printf("Voltage mtrx malloc failed");
	} else {
		zeroedVoltageMtrx(row);
		zeroedVoltageMtrxWoGnd(row - 1);
	}
}

void MatrixStampler::solveForVoltageMatrix() {
	AxEqBSolver(componentMtrxWoGnd, currentMtrxWoGnd,
			column - 1, voltageMtrxWoGnd);
}

void MatrixStampler::zeroedVoltageMtrx(int size) {
	for (int i = 0; i < size; i++) {
		voltageMtrx[i] = 0.0;
	}
}

void MatrixStampler::zeroedVoltageMtrxWoGnd(int size) {
	for (int i = 0; i < size; i++) {
		voltageMtrxWoGnd[i] = 0.0;
	}
}

void MatrixStampler::zeroedCurrentMtrx(int size) {
	for (int i = 0; i < size; i++) {
		currentMtrx[i] = 0.0;
	}
}

void MatrixStampler::zeroedCurrentMtrxWoGnd(int size) {
	for (int i = 0; i < size; i++) {
		currentMtrxWoGnd[i] = 0.0;
	}
}

void MatrixStampler::zeroedComponentMtrx(int size) {
	for (int i = 0; i < size; i++) {
		componentMtrx[i] = 0.0;
	}
}

void MatrixStampler::zeroedComponentMtrxWoGnd(int size) {
	for (int i = 0; i < size; i++) {
		componentMtrxWoGnd[i] = 0.0;
	}
}

void MatrixStampler::printComponentMtrx() {
	int index = 0;
	printf("Component Mtrx: \n");
	for (int i = 0; i < row; i++) {
		printf("[ ");
		for (int j = 0; j < column; j++) {
			printf("(%f) ", componentMtrx[i * column + j]);
		}
		printf("]\n");
	}
	printf("\n");
}

void MatrixStampler::printComponentMtrxWoGnd() {
	int index = 0;
	printf("Component Mtrx w/o Gnd: \n");
	for (int i = 0; i < (row - 1); i++) {
		printf("[ ");
		for (int j = 0; j < (column - 1); j++) {
			printf("(%f) ", componentMtrxWoGnd[i * (column - 1) + j]);
		}
		printf("]\n");
	}
	printf("\n");
}

void MatrixStampler::printCurrentMtrx() {
	printf("Current Mtrx: \n");
	for (int i = 0; i < row; i++) {
		printf("[ %f ]\n", currentMtrx[i]);
	}

}

void MatrixStampler::printCurrentMtrxWoGnd() {
	printf("Current Mtrx w/o gnd: \n");
	for (int i = 0; i < (row - 1); i++) {
		printf("[ %f ]\n", currentMtrxWoGnd[i]);
	}
}

void MatrixStampler::printVoltageMtrx() {
	printf("Voltage Mtrx: \n");
	for (int i = 0; i < row; i++) {
		printf("[ %f ]\n", voltageMtrx[i]);
	}

}

void MatrixStampler::printVoltageMtrxWoGnd() {
	printf("Voltage Mtrx w/o gnd: \n");
	for (int i = 0; i < (row - 1); i++) {
		printf("[ %f ]\n", voltageMtrxWoGnd[i]);
	}

}

void MatrixStampler::truncateGndRowColumn() {
	// Take off row and column of component matrix that's related to gnd
	int index = 0;
	for (int i = 1; i < row; i++) {
		for (int j = 1; j < column; j++) {
			componentMtrxWoGnd[index] = componentMtrx[i * column + j];
			index++;
		}
	}

	// Do the same for current mtrx
	index = 0;
	for (int i = 1; i < row; i++) {
		currentMtrxWoGnd[index] = currentMtrx[i];
		index++;
	}
}
