#include "kernel.h"

int AxEqBSolver(double* aDenseHostPtr, double* bHostPtr, int n, double* xHostPtr) {
	int nnz = 0;
	int * nnzPerRow;

	nnzPerRow = (int *) malloc(n * sizeof(nnzPerRow[0]));
	if (!nnzPerRow) {
		printf("Host malloc failed (nnzPerRow)");
		return 1;
	}

	int * cooRowIndexHostPtr;
	int * cooColIndexHostPtr;
	double * cooValHostPtr;

	getNonZero(aDenseHostPtr, n, nnzPerRow, &nnz); // get number of non zero

	for (int i = 0; i < n; i++) {
		printf("Non zero at row[%d]: %d\n", i, nnzPerRow[i]);
	}
	printf("\nTotal non zero: %d\n\n", nnz);

	cooRowIndexHostPtr = (int *) malloc(nnz * sizeof(cooRowIndexHostPtr[0]));
	cooColIndexHostPtr = (int *) malloc(nnz * sizeof(cooColIndexHostPtr[0]));
	cooValHostPtr = (double *) malloc(nnz * sizeof(cooValHostPtr[0]));

	if ((!cooRowIndexHostPtr) || (!cooColIndexHostPtr) || (!cooValHostPtr)) {
		CLEANUP3("Host malloc failed (matrix)");
		return 1;
	}

	// convert matrix from dense to coo format
	convertDenseToCooFormat(aDenseHostPtr, n, &nnz, cooValHostPtr,
			cooColIndexHostPtr, cooRowIndexHostPtr);

	//print the matrix A
	printf("Input data:\n");
	for (int i = 0; i < nnz; i++) {
		printf("cooRowIndexHostPtr[%d]=%d ", i, cooRowIndexHostPtr[i]);
		printf("cooColIndexHostPtr[%d]=%d ", i, cooColIndexHostPtr[i]);
		printf("cooValHostPtr[%d]=%f \n", i, cooValHostPtr[i]);
	}
	printf("\n");

	linearSysSolverWithLUDec(cooRowIndexHostPtr, cooColIndexHostPtr,
			cooValHostPtr, bHostPtr, n, nnz, xHostPtr);

	return 0;
}

int linearSysSolverWithLUDec(int * cooRowIndexHostPtr, int * cooColIndexHostPtr,
		double * cooValHostPtr, double* bHostPtr, int n, int nnz, double* xHostPtr) {

	// initialize variables for processes' status
	cudaError_t cudaStat1, cudaStat2, cudaStat3, cudaStat4;
	cusparseStatus_t status;

	// initialize variables for handle and matrices' descriptions
	cusparseHandle_t handle = 0;
	cusparseMatDescr_t descr = 0;
	cusparseMatDescr_t descrL = 0;
	cusparseMatDescr_t descrU = 0;

	int * xIndHostPtr = 0;
	double * xValHostPtr = 0;

	double * yHostPtr = 0;

	// initialize variables for device's pointers
	int * cooRowIndex = 0;
	int * cooColIndex = 0;
	double * cooVal = 0;

	int * csrRowPtr = 0;

	int * xInd = 0;
	double * xVal = 0;

	double * x = 0;
	double * b = 0;
	double * y = 0;

	// variables for storing information from csrsv analysis process
	cusparseSolveAnalysisInfo_t info = 0;
	cusparseSolveAnalysisInfo_t inforL = 0;
	cusparseSolveAnalysisInfo_t inforU = 0;

	// The code starts here
	printf("Start solving the linear system, A.x = b...\n\n");
	/*=======================================================================
	 *	Initialize variable in the host
	 *======================================================================*/

	//print the matrix A
	printf("Input data:\n");
	for (int i = 0; i < nnz; i++) {
		printf("cooRowIndexHostPtr[%d]=%d ", i, cooRowIndexHostPtr[i]);
		printf("cooColIndexHostPtr[%d]=%d ", i, cooColIndexHostPtr[i]);
		printf("cooValHostPtr[%d]=%f \n", i, cooValHostPtr[i]);
	}
	printf("\n");

	//create vector y
	yHostPtr = (double *) malloc(n * sizeof(yHostPtr[0]));

	if ((!yHostPtr)) {
		CLEANUP("Host malloc failed (b vectors)");
		return 1;
	}

	/*=======================================================================
	 *	Allocate GPU memory and copy the matrix and vectors into it
	 *======================================================================*/
	cudaStat1 = cudaMalloc((void**) &cooRowIndex, nnz * sizeof(cooRowIndex[0]));
	cudaStat2 = cudaMalloc((void**) &cooColIndex, nnz * sizeof(cooColIndex[0]));
	cudaStat3 = cudaMalloc((void**) &cooVal, nnz * sizeof(cooVal[0]));
	cudaStat4 = cudaMalloc((void**) &b, n * sizeof(b[0]));
	if ((cudaStat1 != cudaSuccess) || (cudaStat2 != cudaSuccess)
			|| (cudaStat3 != cudaSuccess) || (cudaStat4 != cudaSuccess)) {
		CLEANUP("Device malloc failed");
		return 1;
	}

	cudaStat1 = cudaMemcpy(cooRowIndex, cooRowIndexHostPtr,
			(size_t) (nnz * sizeof(cooRowIndex[0])), cudaMemcpyHostToDevice);
	cudaStat2 = cudaMemcpy(cooColIndex, cooColIndexHostPtr,
			(size_t) (nnz * sizeof(cooColIndex[0])), cudaMemcpyHostToDevice);
	cudaStat3 = cudaMemcpy(cooVal, cooValHostPtr,
			(size_t) (nnz * sizeof(cooVal[0])), cudaMemcpyHostToDevice);
	cudaStat4 = cudaMemcpy(b, bHostPtr, (size_t) (n * sizeof(b[0])),
			cudaMemcpyHostToDevice);
	if ((cudaStat1 != cudaSuccess) || (cudaStat2 != cudaSuccess)
			|| (cudaStat3 != cudaSuccess) || (cudaStat4 != cudaSuccess)) {
		CLEANUP("Memcpy from Host to Device failed");
		return 1;
	}

	// reserve memory for x vector on device
	cudaStat1 = cudaMalloc((void**) &x, n * sizeof(x[0]));
	if (cudaStat1 != cudaSuccess) {
		CLEANUP("Device malloc failed (x)");
		return 1;
	}

	cudaStat1 = cudaMemset((void *) x, 0, n * sizeof(x[0]));
	if (cudaStat1 != cudaSuccess) {
		CLEANUP("Memset on Device failed");
		return 1;
	}

	// reserve memory for y vector on device
	cudaStat1 = cudaMalloc((void**) &y, n * sizeof(y[0]));
	if (cudaStat1 != cudaSuccess) {
		CLEANUP("Device malloc failed (y)");
		return 1;
	}

	cudaStat1 = cudaMemset((void *) y, 0, n * sizeof(y[0]));
	if (cudaStat1 != cudaSuccess) {
		CLEANUP("Memset on Device failed");
		return 1;
	}

	cudaDeviceSynchronize();

	/*=======================================================================
	 *	Initialize cusparse
	 *======================================================================*/
	// Initialize cusparse library
	status = cusparseCreate(&handle);
	if (status != CUSPARSE_STATUS_SUCCESS) {
		CLEANUP("CUSPARSE Library initialization failed");
		return 1;
	}

	// create and setup matrix descriptor
	status = cusparseCreateMatDescr(&descr);
	if (status != CUSPARSE_STATUS_SUCCESS) {
		CLEANUP("Matrix descriptor initialization failed");
		return 1;
	}
	cusparseSetMatType(descr, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(descr, CUSPARSE_INDEX_BASE_ZERO);

	// convert matrix from COO to CSR format
	cudaStat1 = cudaMalloc((void**) &csrRowPtr, (n + 1) * sizeof(csrRowPtr[0]));
	if (cudaStat1 != cudaSuccess) {
		CLEANUP("Device malloc failed (csrRowPtr)");
		return 1;
	}
	status = cusparseXcoo2csr(handle, cooRowIndex, nnz, n, csrRowPtr,
			CUSPARSE_INDEX_BASE_ZERO);
	if (status != CUSPARSE_STATUS_SUCCESS) {
		CLEANUP("Conversion from COO to CSR format failed");
		return 1;
	}

	cudaDeviceSynchronize();

	/*=======================================================================
	 *	Run Incomplete LU decomposition
	 *======================================================================*/
	status = cusparseCreateSolveAnalysisInfo(&info);

	// start running phase analysis
	status = cusparseDcsrsv_analysis(handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
			n, nnz, descr, cooVal, csrRowPtr, cooColIndex, info);
	if (status != CUSPARSE_STATUS_SUCCESS) {
		CLEANUP("Phase analysis failed");
		return 1;
	}

	// get incomplete LU decomposition matrix
	status = cusparseDcsrilu0(handle, CUSPARSE_OPERATION_NON_TRANSPOSE, n,
			descr, cooVal, csrRowPtr, cooColIndex, info);
	if (status != CUSPARSE_STATUS_SUCCESS) {
		CLEANUP("LU factorization failed");
		return 1;
	}

	cudaDeviceSynchronize();

	// print LU Factorization results (cooVal)
	cudaStat1 = cudaMemcpy(cooValHostPtr, cooVal,
			(size_t) (nnz * sizeof(cooVal[0])), cudaMemcpyDeviceToHost);
	if (cudaStat1 != cudaSuccess) {
		CLEANUP("Memcpy from Device to Host failed");
		return 1;
	}

	printf("MatrixLU:\n");
	for (int i = 0; i < nnz; i++) {
		printf("cooRowIndexHostPtr[%d]=%d ", i, cooRowIndexHostPtr[i]);
		printf("cooColIndexHostPtr[%d]=%d ", i, cooColIndexHostPtr[i]);
		printf("cooValHostPtr[%d]=%f \n", i, cooValHostPtr[i]);
	}
	printf("\n");

	cudaDeviceSynchronize();

	/*=======================================================================
	 *	Solve linear system, A.x = b using LU decomposition
	 *	1) Solve L.y = b
	 *	2) Then, solve U.x = y
	 *======================================================================*/
	//create lower matrix descriptor
	status = cusparseCreateMatDescr(&descrL);
	if (status != CUSPARSE_STATUS_SUCCESS) {
		CLEANUP("Lower matrix descriptor initialization failed");
		return 1;
	}
	cusparseSetMatType(descrL, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(descrL, CUSPARSE_INDEX_BASE_ZERO);
	cusparseSetMatDiagType(descrL, CUSPARSE_DIAG_TYPE_UNIT);
	cusparseSetMatFillMode(descrL, CUSPARSE_FILL_MODE_LOWER);

	//create upper matrix descriptor
	status = cusparseCreateMatDescr(&descrU);
	if (status != CUSPARSE_STATUS_SUCCESS) {
		CLEANUP("Upper matrix descriptor initialization failed");
		return 1;
	}
	cusparseSetMatType(descrU, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(descrU, CUSPARSE_INDEX_BASE_ZERO);
	cusparseSetMatDiagType(descrU, CUSPARSE_DIAG_TYPE_NON_UNIT);
	cusparseSetMatFillMode(descrU, CUSPARSE_FILL_MODE_UPPER);

	//create lower matrix analysis info
	status = cusparseCreateSolveAnalysisInfo(&inforL);
	if (status != CUSPARSE_STATUS_SUCCESS) {
		CLEANUP("Create lower matrix analysis info failed");
		return 1;
	}
	//create upper matrix analysis info
	status = cusparseCreateSolveAnalysisInfo(&inforU);
	if (status != CUSPARSE_STATUS_SUCCESS) {
		CLEANUP("Create upper matrix analysis info failed");
		return 1;
	}

	status = cusparseDcsrsv_analysis(handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
			n, nnz, descrL, cooVal, csrRowPtr, cooColIndex, inforL);
	if (status != CUSPARSE_STATUS_SUCCESS) {
		printf("%s \n\n", "cusparseDcsrsv_analysis1 Error !");
	}

	status = cusparseDcsrsv_analysis(handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
			n, nnz, descrU, cooVal, csrRowPtr, cooColIndex, inforU);
	if (status != CUSPARSE_STATUS_SUCCESS)
		printf("%s \n\n", "cusparseDcsrsv_analysis2 Error !");

	cudaDeviceSynchronize();

	// Solve L.y = b
	const double alpha = 1.0;
	cusparseDcsrsv_solve(handle, CUSPARSE_OPERATION_NON_TRANSPOSE, n, &alpha,
			descrL, cooVal, csrRowPtr, cooColIndex, inforL, b, y);

	cudaDeviceSynchronize();

	// Print L.y = b results
	cudaStat1 = cudaMemcpy(yHostPtr, y, (size_t) (n * sizeof(y[0])),
			cudaMemcpyDeviceToHost);
	if (cudaStat1 != cudaSuccess) {
		CLEANUP("Memcpy from Device to Host failed");
		return 1;
	}

	printf("Vector y from L.y = b:\n");
	for (int i = 0; i < n; i++) {
		printf("yHostPtr[%d]=%f ", i, yHostPtr[i]);
	}
	printf("\n\n");

	// Solve U.x = y
	cusparseDcsrsv_solve(handle, CUSPARSE_OPERATION_NON_TRANSPOSE, n, &alpha,
			descrU, cooVal, csrRowPtr, cooColIndex, inforU, y, x);

	cudaDeviceSynchronize();

	// Print U.x = y results
	cudaStat1 = cudaMemcpy(xHostPtr, x, (size_t) (n * sizeof(x[0])),
			cudaMemcpyDeviceToHost);
	if (cudaStat1 != cudaSuccess) {
		CLEANUP("Memcpy from Device to Host failed");
		return 1;
	}

	printf("Vector x from U.x = y:\n");
	for (int i = 0; i < n; i++) {
		printf("xHostPtr[%d]=%f ", i, xHostPtr[i]);
	}
	printf("\n\n");

	/*=======================================================================
	 *	Perform clean up and result checking here
	 *	1) Solve L.y = b
	 *	2) Then, solve U.x = y
	 *======================================================================*/
	cudaDeviceSynchronize();

	// destroy matrix descriptor
	status = cusparseDestroyMatDescr(descr);
	descr = 0;
	if (status != CUSPARSE_STATUS_SUCCESS) {
		CLEANUP("Matrix descriptor destruction failed");
		return 1;
	}

	/* destroy handle */
	status = cusparseDestroy(handle);
	handle = 0;
	if (status != CUSPARSE_STATUS_SUCCESS) {
		CLEANUP("CUSPARSE Library release of resources failed");
		return 1;
	}

	return 0;

}

int getNonZero(double * aHostPtr, int n, int rowNnz[], int * totalNnz) {
	double * aDense = 0; // initialize variable for device ptr
	int *nnzPerRow = 0;
	int *nnzTotalDevPtr = 0;
	cudaError_t cudaStat;
	cusparseHandle_t handle = 0;
	cusparseMatDescr_t descr = 0;
	cusparseStatus_t status;
	int * temp1 = 0;
	int * temp2 = 0;

	// reserve memory for matrix a on device
	cudaStat = cudaMalloc((void**) &aDense, n * n * sizeof(aDense[0]));
	if (cudaStat != cudaSuccess) {
		CLEANUP2("Device malloc failed (a)");
		return 1;
	}
	// copy data from host to device
	cudaStat = cudaMemcpy(aDense, aHostPtr,
			(size_t) (n * n * sizeof(aDense[0])), cudaMemcpyHostToDevice);
	if (cudaStat != cudaSuccess) {
		CLEANUP2("Memcpy from Host to Device failed");
		return 1;
	}

	// Initialize cusparse library
	status = cusparseCreate(&handle);
	if (status != CUSPARSE_STATUS_SUCCESS) {
		CLEANUP2("CUSPARSE Library initialization failed");
		return 1;
	}

	// create and setup matrix descriptor
	status = cusparseCreateMatDescr(&descr);
	if (status != CUSPARSE_STATUS_SUCCESS) {
		CLEANUP2("Matrix descriptor initialization failed");
		return 1;
	}
	cusparseSetMatType(descr, CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(descr, CUSPARSE_INDEX_BASE_ZERO);

	// allocate memory to store nnzPerRow and nnzTotalDevPtr
	cudaStat = cudaMalloc((void**) &nnzPerRow, n * n * sizeof(nnzPerRow[0]));
	if (cudaStat != cudaSuccess) {
		CLEANUP2("Device malloc failed (nnzPerRowColumn)");
		return 1;
	}
	cudaStat = cudaMalloc((void**) &nnzTotalDevPtr, sizeof(nnzTotalDevPtr[0]));
	if (cudaStat != cudaSuccess) {
		CLEANUP2("Device malloc failed (nnzTotalDevPtr)");
		return 1;
	}

	// get the number of non zero in the input matrix
	status = cusparseDnnz(handle, CUSPARSE_DIRECTION_ROW, n, n, descr, aDense,
			n, nnzPerRow, nnzTotalDevPtr);
	if (status != CUSPARSE_STATUS_SUCCESS) {
		CLEANUP2("NNZ function failed");
	}

	// copy value back to host
	temp1 = (int *) malloc(sizeof(temp1[0]));
	cudaStat = cudaMemcpy(temp1, nnzPerRow, n * n * sizeof(nnzPerRow[0]),
			cudaMemcpyDeviceToHost);
	if (cudaStat != cudaSuccess) {
		CLEANUP2("Memcpy from Device to Host failed");
		return 1;
	}

	temp2 = (int *) malloc(sizeof(temp2[0]));
	cudaStat = cudaMemcpy(temp2, nnzTotalDevPtr, sizeof(nnzTotalDevPtr[0]),
			cudaMemcpyDeviceToHost);
	if (cudaStat != cudaSuccess) {
		CLEANUP2("Memcpy from Device to Host failed");
		return 1;
	}

	for (int i = 0; i < n; i++) {
		//printf("\tNon zero at row[%d]: %d\n", i, temp1[i]);
		rowNnz[i] = temp1[i];
	}
	//printf("Total non zero: %d", temp2[0]);
	*totalNnz = temp2[0];

	// destroy matrix descriptor
	status = cusparseDestroyMatDescr(descr);
	descr = 0;
	if (status != CUSPARSE_STATUS_SUCCESS) {
		CLEANUP2("Matrix descriptor destruction failed");
		return 1;
	}

	/* destroy handle */
	status = cusparseDestroy(handle);
	handle = 0;
	if (status != CUSPARSE_STATUS_SUCCESS) {
		CLEANUP2("CUSPARSE Library release of resources failed");
		return 1;
	}

//	// perform memory clean up here after all operation are done
//	CLEANUP2("Cleaning memory...");

	return 0;

}

int convertDenseToCooFormat(double * inputDenseMtrx, int n, int * nnz,
		double* cooVal, int* cooColIndex, int* cooRowIndex) {

	if (inputDenseMtrx == NULL) {
		printf("ERROR:Input Dense Matrix is Null");
		return -1;
	}

	// start converting from dense to coo format
	int cooIdx = 0;
	int denseIdx = 0;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (inputDenseMtrx[denseIdx] != 0.0) {
				cooRowIndex[cooIdx] = i;
				cooColIndex[cooIdx] = j;
				cooVal[cooIdx] = inputDenseMtrx[denseIdx];
				cooIdx++;
			}
			denseIdx++;
		}

	}

	return 0;
}

